### 1. package.json 中的 { configs } 项为项目定制配置

```text
项目配置
configs                                 {}
configs.companyName                     String      公司全称
configs.website                         String      主页
configs.appCopyright                    String      版权信息
configs.bossKey                         String      默认显隐键
configs.logMaxSize                      Number      日志最大体积（字节）
configs.settingDir                      String      用户设置目录

开发环境配置
configs.develop                         {}
configs.develop.host                    String      vue调试服务器域名/ip
configs.develop.port                    Number      vue调试服务器端口

生产环境
configs.production                      {}
configs.production.tempDir              String      临时目录
configs.production.buildDir             String      vue发布目录
configs.production.indexFile            String      首页文件，默认index.html

图标
configs.icon                            {}
configs.icon.normal                     String      默认图标
configs.icon.disabled                   String      禁用状态图标

主窗口
configs.windowMain                      {}
configs.windowMain.minWidth             Number      最小、初始化宽度
configs.windowMain.minHeight            Number      最小、初始化高度"

electron打包配置
configs.electron                        {}
configs.electron.platform               String      平台 win32
configs.electron.arch                   Number      平台位数 32|64
configs.electron.version                String      electron目标打包版本
configs.electron.out                    String      打包输出目录
configs.electron.asar                   Boolean     是否打包资源
configs.electron.requireAdministrator   Boolean     是否提升管理员权限

Inno Setup 配置
configs.inno                            {}
configs.inno.iscc                       String      Inno命令行工具路径
configs.inno.out                        String      输出目录
configs.inno.appId                      {}
configs.inno.appId.x64                  String      64位安装包appid
configs.inno.appId.ia32                 String      32位安装包appid

feed 配置
feed                                    {}
feed.updater                            String      更新订阅地址
feed.webviews                           String      webview配置订阅地址
feed.plugins                            String      插件配置订阅地址
```

### 示例

```json
{
    "..."     : "...",
    "configs" : {
        "companyName"  : "Exi Design",
        "website"      : "https://exi.red",
        "appCopyright" : "Copyright © Exi Design <1360666669@qq.com>",
        "develop"      : {
            "host" : "127.0.0.1",
            "port" : 9191
        },
        "production"   : {
            "tempDir"   : "dist/temp",
            "buildDir"  : "dist/vue",
            "indexFile" : "index.html"
        },
        "bossKey"      : "ALT+W",
        "icon"         : {
            "normal"   : "static/icons/icon.ico",
            "disabled" : "static/icons/icon-disabled.ico"
        },
        "windowMain"   : {
            "minWidth"  : 1050,
            "minHeight" : 600
        },
        "settingDir"   : "setting",
        "logMaxSize"   : 10485760,
        "electron"     : {
            "platform"             : "win32",
            "arch"                 : 64,
            "version"              : "13.1.8",
            "out"                  : "dist/electron",
            "asar"                 : false,
            "requireAdministrator" : false
        },
        "inno"         : {
            "iscc"  : "D:/soft/Inno Setup 6/ISCC.exe",
            "out"   : "dist/setup",
            "appId" : {
                "x64"  : "00000000-0000-0000-0000-000000000000",
                "ia32" : "99999999-9999-9999-9999-999999999999"
            }
        },
        "feed"         : {
            "updater" : "https://domain/exi-kit-feed.json",
            "webview" : "https://domain/exi-kit-webview.json",
            "plugins" : "https://domain/exi-kit-plugins.json"
        }
    },
    "..."     : "..."
}
```
