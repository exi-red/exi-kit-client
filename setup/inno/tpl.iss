; 软件名称
#define CusAppName "{$CusAppName}"

; 版本号
#define CusAppVersion "{$CusAppVersion}"

; 软件名称
#define CusAppId "{$CusAppId}"

; 安装图标
#define CusAppIcon "{$CusAppIcon}"

; 架构
#define CusArchFlag "{$CusArchFlag}"

; 发布者
#define CusAppPublisher "{$CusAppPublisher}"

; 项目地址
#define CusAppURL "{$CusAppURL}"

; 项目路径
#define CusProjectPath "{$CusProjectPath}"

; 资源目录
#define CusResourceDir "{$CusResourceDir}"

; 打包生成路径
#define CusOutDir "{$CusOutDir}"

[Setup]
; 安装目录
DefaultDirName=C:\{#CusAppName}-{#CusArchFlag}
AppId={#CusAppId}
AppName={#CusAppName}
AppVersion={#CusAppVersion}
AppPublisher={#CusAppPublisher}
AppPublisherURL={#CusAppURL}
AppSupportURL={#CusAppURL}
AppUpdatesURL={#CusAppURL}
DisableProgramGroupPage=yes
LicenseFile={#CusProjectPath}\setup\release-licence-gbk.txt

; 添加管理员权限
; PrivilegesRequired=admin
; 安装目录，找到文件SetupLdr.e32 用 ResHacker.exe
; 找到 <requestedExecutionLevel level="asInvoker" uiAccess="false"/></requestedPrivileges>
; 改为 <requestedExecutionLevel level="requireAdministrator" uiAccess="false"/></requestedPrivileges>
;
; 清理管理员权限用 none 或 lowest
; SetupLdr.e32 中 requireAdministrator 改回 asInvoker
; 注释 PrivilegesRequiredOverridesAllowed=commandline

PrivilegesRequired=lowest
;PrivilegesRequiredOverridesAllowed=commandline

OutputDir={#CusOutDir}
OutputBaseFilename={#CusAppName}-Setup-{#CusArchFlag}-v{#CusAppVersion}
SetupIconFile={#CusAppIcon}
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "chinesesimplified"; MessagesFile: "compiler:Languages\ChineseSimplified.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone

; CurUninstallStepChanged 删除所有配置文件以达到干净卸载的目的
; InitializeWizard        默认同意协议
[code]
procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
    if CurUninstallStep = usUninstall then
         if MsgBox('是否要删除用户配置信息？', mbConfirmation, MB_YESNO) = IDYES then
            DelTree(ExpandConstant('{app}'), True, True, True);
end;

procedure InitializeWizard();
begin
  WizardForm.LICENSEACCEPTEDRADIO.Checked:=true;
end;

[Files]
Source: "{#CusResourceDir}\{#CusAppName}-win32-{#CusArchFlag}\*"; Excludes: ""; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{autoprograms}\{#CusAppName}-{#CusArchFlag}"; Filename: "{app}\{#CusAppName}.exe"
Name: "{autodesktop}\{#CusAppName}-{#CusArchFlag}"; Filename: "{app}\{#CusAppName}.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\{#CusAppName}.exe"; Description: "{cm:LaunchProgram,{#StringChange(CusAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
