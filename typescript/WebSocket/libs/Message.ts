import { Cmd }     from './Cmd';
import { Channel } from './Channel';

/**
 * WebSocket消息格式
 */
export interface Message {

    /**
     * uuid
     */
    uuid? : string;

    /**
     * 频道
     */
    channel : Channel;

    /**
     * 命令
     */
    cmd : Cmd;

    /**
     * 传输数据
     */
    data : any;
}
