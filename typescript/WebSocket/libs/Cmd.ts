/**
 * WebSocket消息命令
 */
export enum Cmd {
    USER = 'USER',
    NAME = 'NAME'
}
