/*
 * -------------------------------------------------------------------------
 * 入口文件
 * -------------------------------------------------------------------------
 */

const app                 = require('./src/app/instance');
const logger              = require('./src/logger');
const appPaths            = require('./src/app/paths');
const shortcutGlobal      = require('./src/shortcut/global');
const appEvents           = require('./src/app/events');
const windowMain          = require('./src/window/main');
const windowSwitch        = require('./src/window/switch');
const tray                = require('./src/tray');
const menu                = require('./src/menu');
const renderWindowControl = require('./src/render/windowControl');
const renderSetting       = require('./src/render/setting');
const renderDialog        = require('./src/render/dialog');
const updater             = require('./src/updater');
const setting             = require('./src/setting');
const clearAppData        = require('./src/clearAppData');
const plugins             = require('./src/plugins');
const webviews            = require('./src/webviews');

/*app就绪，启动窗口*/
app.on('ready', () => {
    /*初始化 app目录管理*/
    appPaths.initialize(app);
    
    /*初始化 日志管理器*/
    logger.initialize(app);
    
    /*初始化 全局快捷键*/
    shortcutGlobal.initialize(app);
    
    /*初始化 app事件管理器*/
    appEvents.initialize(app);
    
    /*初始化 主窗口*/
    windowMain.initialize(app);
    
    /*初始化 窗口显隐切换*/
    windowSwitch.initialize(app);
    
    /*初始化 托盘*/
    tray.initialize(app);
    
    /*初始化 菜单*/
    menu.initialize(app);
    
    /*初始化 渲染进程的窗口控制*/
    renderWindowControl.initialize(app);
    
    /*初始化 配置管理器*/
    setting.initialize(app);
    
    /*初始化 渲染进程配置管理器*/
    renderSetting.initialize(app);
    
    /*初始化 渲染进程对话框*/
    renderDialog.initialize(app);
    
    /*初始化 升级管理器*/
    updater.initialize(app);
    
    /*初始化 插件管理器*/
    plugins.initialize(app);
    
    /*初始化 webview管理器*/
    webviews.initialize(app);
    
    /*初始化 默认appData目录清理工具*/
    clearAppData.initialize(app);
});
