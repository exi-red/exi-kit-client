// const fs  = require('fs');
// const app = require('electron').app;
// app.on('ready', () => {
//     // app.getFileIcon('%SystemRoot%\\System32\\SHELL32.dll').then(r => {
//     //     fs.writeFileSync('1.png', r.toPNG());
//     //     process.exit();
//     // });
//     console.log(app.getAppMetrics());
//
// //    wmic path Win32_PerfFormattedData_PerfProc_Process where name="idle" get Name,PercentProcessorTime,IDProcess
// });

const { spawn } = require('child_process');
const _         = require('lodash');
const os        = require('os');

/*执行命令*/
let ps = spawn('cmd.exe', ['/s', '/c', `chcp 65001 && wmic path Win32_PerfFormattedData_PerfProc_Process where name="idle" get Name,PercentProcessorTime,IDProcess`]);

/*捕获标准输出并将其打印到控制台*/
ps.stdout.on('data', data => console.log(_.trim(data.toString())));

/*捕获标准错误输出并将其打印到控制台*/
ps.stderr.on('data', data => console.log(_.trim(data.toString())));

/*注册子进程关闭事件*/
ps.on('exit', (code, signal) => (code && console.log(`${os.EOL}[Abnormal exit]`, code, signal || '')));
