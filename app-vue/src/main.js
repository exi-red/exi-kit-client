// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';

/*字体*/
import './assets/fonts/iconfont.css';

/*自动引入文件或组件*/
import './plugins/auto-require';

/*复制方法（需要 clipboard 和 dialog 引入成功后）*/
Vue.prototype.$copy = (message) => {
    let config = { closeTime: 1000 };
    
    if (!message) {
        Vue.prototype.$messageWarning('没有要复制的内容', config);
        return;
    }
    
    Vue.prototype.$clipboard(message)
        ? Vue.prototype.$messageSuccess('复制成功', config)
        : Vue.prototype.$messageWarning('复制失败', config);
};

Vue.config.productionTip = process.env.NODE_ENV === 'production';

new Vue({
    el        : '#app',
    router,
    store,
    components: { App },
    template  : '<App/>'
});
