import Vue from 'vue';
import Router from 'vue-router';
import store from '../store/index';

Vue.use(Router);

/*解决 Uncaught (in promise)*/
const originalPush    = Router.prototype.push;
Router.prototype.push = function (location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject);
    return originalPush.call(this, location).catch(err => err);
};

/*路由配置*/
const MyRouter = new Router({
    mode  : 'hash',
    routes: [
        /*首页*/
        {
            path     : '/',
            name     : 'index',
            alias    : ['/index'],
            component: () => import('../components/index').then(m => m.default)
        },
        /*时间戳*/
        {
            path     : '/kit/timestamp',
            name     : 'kit_timestamp',
            component: () => import('../components/kit/timestamp').then(m => m.default)
        },
        /*随机字符串*/
        {
            path     : '/kit/randomString',
            name     : 'kit_randomString',
            component: () => import('../components/kit/randomString').then(m => m.default)
        },
        /*JSON*/
        {
            path     : '/kit/json',
            name     : 'kit_json',
            component: () => import('../components/kit/json').then(m => m.default)
        },
        /*MD5*/
        {
            path     : '/kit/md5',
            name     : 'kit_md5',
            component: () => import('../components/kit/md5').then(m => m.default)
        },
        /*BASE64*/
        {
            path     : '/kit/base64',
            name     : 'kit_base64',
            component: () => import('../components/kit/base64').then(m => m.default)
        },
        /*UTF8*/
        {
            path     : '/kit/unicode',
            name     : 'kit_unicode',
            component: () => import('../components/kit/unicode').then(m => m.default)
        },
        /*百度翻译*/
        {
            path     : '/kit/baiduTranslate',
            name     : 'kit_baidu_translate',
            component: () => import('../components/kit/baiduTranslate').then(m => m.default)
        },
        /*任务管理器*/
        {
            path     : '/kit/tasklist',
            name     : 'kit_tasklist',
            component: () => import('../components/kit/tasklist').then(m => m.default)
        },
        /*插件*/
        {
            path     : '/kit/plugin',
            name     : 'kit_plugin',
            component: () => import('../components/kit/plugin').then(m => m.default)
        },
        /*404页面*/
        {
            path     : '*',
            name     : 'err404',
            component: () => import('../components/404').then(m => m.default)
        }
    ]
});

/*路由卫士*/
MyRouter.beforeEach((to, from, next) => {
    Vue.prototype.$bodyTitle = store.getters['menu/getMenu'](to.name);
    to.name && store.commit('menu/setActiveIndex', to.name);
    next();
});

/**
 * 导出
 */
export default MyRouter;
