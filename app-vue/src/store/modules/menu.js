import _ from 'lodash';

/*状态树*/
const state = () => ({
    /*菜单*/
    lists: [
        {
            title      : '首页',
            description: '首页',
            name       : 'index',
            to         : '/index',
            icon       : 'appfont app-home'
        },
        {
            title      : 'JSON',
            description: 'JSON编辑器',
            name       : 'kit_json',
            to         : '/kit/json',
            icon       : 'appfont app-json'
        },
        {
            title      : '时间戳',
            description: '时间戳转换',
            name       : 'kit_timestamp',
            to         : '/kit/timestamp',
            icon       : 'appfont app-count-down'
        },
        {
            title      : '随机字符串',
            description: '随机字符串',
            name       : 'kit_randomString',
            to         : '/kit/randomString',
            icon       : 'appfont app-convert2'
        },
        {
            title      : 'MD5',
            description: 'MD5转码',
            name       : 'kit_md5',
            to         : '/kit/md5',
            icon       : 'appfont app-password'
        },
        {
            title      : 'BASE64',
            description: 'BASE64转码',
            name       : 'kit_base64',
            to         : '/kit/base64',
            icon       : 'appfont app-code2'
        },
        {
            title      : 'UNICODE',
            description: 'UNICODE转码',
            name       : 'kit_unicode',
            to         : '/kit/unicode',
            icon       : 'appfont app-code3'
        },
        {
            title      : '百度翻译',
            description: '百度翻译',
            name       : 'kit_baidu_translate',
            to         : '/kit/baiduTranslate',
            icon       : 'appfont app-baidu-translate'
        },
        {
            title      : '任务管理器',
            description: '任务管理器',
            name       : 'kit_tasklist',
            to         : '/kit/tasklist',
            icon       : 'appfont app-renwu'
        }
        // ,{
        //     separator: true
        // }
        //, {
        //     title      : '插件',
        //     description: '插件',
        //     name       : 'kit_plugin',
        //     to         : '/kit/plugin',
        //     icon       : 'appfont app-plugin'
        // }
    ],
    
    /*激活的菜单序号*/
    activeIndex: 0
});

/*派生状态*/
const getters = {
    lists: state => {
        return state.lists;
    },
    
    activeIndex: state => {
        return state.activeIndex;
    },
    
    getMenu: state => flag => {
        let index = _.findIndex(state.lists, { 'name': flag });
        return state.lists[index < 0 ? 0 : index];
    }
};

/*事件*/
const actions = {
    setActiveIndex ({ commit, state }, flag) {
        commit('setActiveIndex', flag);
    }
};

/*操作*/
const mutations = {
    setActiveIndex (state, flag) {
        let index = flag;
        /*字符串*/
        if (flag === (flag + '')) {
            index = _.findIndex(state.lists, { 'name': flag });
        }
        state.activeIndex = index;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
