/*状态树*/
const state = () => ({ setting: {} });

/*派生状态*/
const getters = {
    setting: state => state.setting
};

/*事件*/
const actions = {
    update: ({ commit, state }, setting) => commit('update', setting)
};

/*操作*/
const mutations = {
    update: (state, setting) => state.setting = setting
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
