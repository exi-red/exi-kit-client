import _ from 'lodash';

/**
 * 捕捉组合键
 * @param i
 */
function watch (i) {
    /*键名*/
    let keyName = i.key;
    
    /*映射electron支持的快捷键名称*/
    const electronMap = {
        32 : 'Space',
        27 : 'Esc',
        37 : 'Left',
        38 : 'Up',
        39 : 'Right',
        40 : 'Down',
        106: 'Nummult',
        107: 'Numadd',
        109: 'Numsub',
        110: 'Numdec',
        111: 'Numdiv',
        96 : 'Num0',
        97 : 'Num1',
        98 : 'Num2',
        99 : 'Num3',
        100: 'Num4',
        101: 'Num5',
        102: 'Num6',
        103: 'Num7',
        104: 'Num8',
        105: 'Num9'
    };
    
    /*空格键 Space => code 32*/
    const isSpace = (i.keyCode === 32);
    
    /*Esc键 => code 27*/
    const isEsc = (i.keyCode === 27);
    
    /*F键 F1~F24 => code 112~135*/
    const isFKey = _.inRange(i.keyCode, 111, 136);
    
    /*数字键 0~9 => code 48~57*/
    const isNumber = _.inRange(i.keyCode, 47, 58);
    
    /*字母键 a~z(A~Z) => code 65~90*/
    const isLetter = _.inRange(i.keyCode, 64, 91);
    
    /**
     * 方向键
     * Left  => 37
     * Up    => 38
     * Right => 39
     * Down  => 40
     */
    const isDirection = _.inRange(i.keyCode, 36, 41);
    
    /**
     * 控制区键
     * PageUp   => 33
     * PageDown => 34
     * End      => 35
     * Home     => 36
     * Insert   => 45
     * Delete   => 46
     */
    const isControlArea = (_.indexOf([33, 34, 35, 36, 45, 46], i.keyCode) !== -1);
    
    /**
     * 小键盘区符号键
     * * => 106
     * + => 107
     * - => 109
     * . => 110
     * / => 111
     */
    const isNumpadSymbol = (_.indexOf([106, 107, 109, 110, 111], i.keyCode) !== -1);
    
    /*小键盘区数字键 => code 96~105*/
    const isNumpad = _.inRange(i.keyCode, 95, 106);
    
    /**
     * 主键盘区符号键
     * ; => 186 => Semicolon
     * = => 187 => Equal
     * , => 188 => Comma
     * - => 189 => Minus
     * . => 190 => Period
     * / => 191 => Slash
     * ` => 192 => Backquote
     * [ => 219 => BracketLeft
     * \ => 220 => Backslash
     * ] => 221 => BracketRight
     * ' => 222 => Quote
     */
    const isSymbol = (_.indexOf([186, 187, 188, 189, 190, 191, 192, 219, 220, 221, 222], i.keyCode) !== -1);
    
    /*拼装快捷键*/
    let bossKey = '';
    
    /*F键*/
    if (isFKey) bossKey = addFunctionKey(i, keyName, 'F');
    
    /*其他支持的按钮*/
    else if (isSpace || isEsc || isNumber || isLetter || isDirection || isControlArea || isNumpadSymbol || isNumpad || isSymbol) {
        /*键名重命名，使keyName符合electron快捷键设置标准*/
        keyName = electronMap[i.keyCode] || keyName;
        bossKey = addFunctionKey(i, keyName);
    }
    
    return bossKey.toUpperCase();
}

/**
 * 添加功能键
 * @param i
 * @param name
 * @param mode
 */
function addFunctionKey (i, name, mode) {
    let keys = [];
    
    /*win键*/
    i.metaKey && keys.push('Super');
    
    /*ctrl键*/
    i.ctrlKey && keys.push('Ctrl');
    
    /*shift键*/
    i.shiftKey && keys.push('Shift');
    
    /*alt键*/
    i.altKey && keys.push('Alt');
    
    /*F键模式，支持无功能键*/
    if (mode === 'F') keys.push(name);
    
    /*其他键必须有功能键*/
    else if (i.metaKey || i.ctrlKey || i.shiftKey || i.altKey) {
        keys.push(name);
        if (keys.length === 1) {
            keys = [];
        }
    }
    
    /*无效键*/
    else (keys = []);
    
    return keys.join('+');
}

/**
 * Win快捷键名称转换到electron可识名称
 * @param keyName
 * @returns {*}
 */
function win2super (keyName) {
    return keyName ? keyName.replace('WIN', 'SUPER') : '';
}

/**
 * electron可识名称转换到Win快捷键名称
 * @param keyName
 * @returns {*}
 */
function super2win (keyName) {
    return keyName ? keyName.replace('SUPER', 'WIN') : '';
}

/**
 * 导出
 */
export default {
    watch,
    win2super,
    super2win
};
