import Vue from 'vue';
import is from '../electron/helper/is';
import IPC from '../electron/ipc/dict';

/*别名*/
const IPC_SETTING = IPC.IPC_SETTING;

Vue.use({
    install (Vue) {
        /*设置管理器*/
        Vue.prototype.$settingManager = {
            /**
             * 异步读取
             */
            read: () => ipcRenderer.send(IPC_SETTING.GET),
            
            /**
             * 异步更新
             * @param data
             */
            update: data => ipcRenderer.send(IPC_SETTING.UPDATE, data),
            
            /**
             * 接收到app设置获取通知
             * @param cb
             * @returns {Electron.IpcRenderer}
             */
            listenSetting: cb => ipcRenderer.on(IPC_SETTING.NOTIFY, (e, data) => is.func(cb) && cb(data)),
            
            /**
             * 接收到异步设置结果
             * @param cb
             * @returns {Electron.IpcRenderer}
             */
            listenSettingUpdate: cb => ipcRenderer.on(IPC_SETTING.UPDATE_NOTIFY, (e, isSuccess) => is.func(cb) && cb(isSuccess))
        };
    }
});
