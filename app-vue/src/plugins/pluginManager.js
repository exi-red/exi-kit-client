import Vue from 'vue';
import is from '../electron/helper/is';
import IPC from '../electron/ipc/dict';

/*别名*/
const IPC_PLUGIN = IPC.IPC_PLUGIN;

Vue.use({
    install (Vue) {
        /*插件管理器*/
        Vue.prototype.$pluginManager = {
            /**
             * 读取列表
             */
            getlist: () => ipcRenderer.send(IPC_PLUGIN.GETLISTS),
            
            /**
             * 执行插件
             * @param data
             */
            call: data => ipcRenderer.send(IPC_PLUGIN.EXECUTIVE, data),
            
            /**
             * 接收到列表通知
             * @param cb
             * @return {Electron.IpcRenderer}
             */
            receivelist: cb => ipcRenderer.on(IPC_PLUGIN.PUTLISTS, (e, data) => is.func(cb) && cb(data)),
            
            /**
             * 接收到异步设置结果
             * @param cb
             * @returns {Electron.IpcRenderer}
             */
            receiveExecutiveResult: cb => ipcRenderer.on(IPC_PLUGIN.EXECUTIVE_RESULT, (e, isSuccess) => is.func(cb) && cb(isSuccess))
        };
    }
});
