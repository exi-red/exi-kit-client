import Vue from 'vue';
import is from '../../../helper/is';
import dialogBox from '../components/install/dialogBox';

Vue.use({
    install (Vue) {
        /*创建一个子类*/
        const DialogConstructor = Vue.extend(dialogBox);

        /*实例化这个子类*/
        const instance = new DialogConstructor();

        /*创建一个div元素，并把实例挂载到div元素上*/
        instance.$mount(document.createElement('div'));

        /*将div插入到body元素中*/
        document.body.appendChild(instance.$el);

        /*信息提示方法*/
        Vue.prototype.$message = (message, config) => {
            config = config || {};

            instance.type        = 'message';
            instance.isShow      = true;
            instance.title       = '';
            instance.message     = message || '';
            instance.muskLock    = config.muskLock || false;
            instance.closeTime   = config.closeTime || 1000;
            instance.muskOpacity = config.muskOpacity || 0.5;
        };

        /*信息提示方法（成功）*/
        Vue.prototype.$messageSuccess = (message, config) => {
            config = config || {};

            instance.type        = 'message';
            instance.messageIcon = 'success';
            instance.isShow      = true;
            instance.title       = '';
            instance.message     = message || '';
            instance.muskLock    = config.muskLock || false;
            instance.closeTime   = config.closeTime || 1000;
            instance.muskOpacity = config.muskOpacity || 0.5;
        };

        /*信息提示方法（警告）*/
        Vue.prototype.$messageWarning = (message, config) => {
            config = config || {};

            instance.type        = 'message';
            instance.messageIcon = 'warning';
            instance.isShow      = true;
            instance.title       = '';
            instance.message     = message || '';
            instance.muskLock    = config.muskLock || false;
            instance.closeTime   = config.closeTime || 1000;
            instance.muskOpacity = config.muskOpacity || 0.5;
        };

        /*alert提示方法*/
        Vue.prototype.$alert = (message, config) => {
            config = config || {};

            instance.type        = 'alert';
            instance.isShow      = true;
            instance.muskLock    = true;
            instance.message     = message || '';
            instance.title       = config.title || '消息提示';
            instance.btnConfirm  = config.btnConfirm || '确定';
            instance.muskOpacity = config.muskOpacity || 0.5;
            instance.closeTime   = 0;

            is.func(config.success) && (instance.success = config.success);
        };

        /*confirm确认方法*/
        Vue.prototype.$confirm = (message, config) => {
            config = config || {};

            instance.type        = 'confirm';
            instance.isShow      = true;
            instance.message     = message || '';
            instance.title       = config.title || '操作确认';
            instance.btnConfirm  = config.btnConfirm || '确定';
            instance.btnCancel   = config.btnCancel || '取消';
            instance.muskLock    = config.muskLock || true;
            instance.muskOpacity = config.muskOpacity || 0.5;
            instance.closeTime   = 0;

            is.func(config.success) && (instance.success = config.success);
            is.func(config.cancel) && (instance.cancel = config.cancel);
        };
    }
});
