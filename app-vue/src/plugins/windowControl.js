import Vue from 'vue';
import IPC from '../electron/ipc/dict';

/*别名*/
const IPC_WINDOW_CONTROL = IPC.IPC_WINDOW_CONTROL;

Vue.use({
    install (Vue) {
        /*软件UI窗口控制*/
        Vue.prototype.$windowControl = {
            /*最小化*/
            minimize: () => ipcRenderer.send(IPC_WINDOW_CONTROL.RENDER_CONTROL, IPC_WINDOW_CONTROL.MINIMIZE),
            
            /*最大化*/
            maximize: () => ipcRenderer.send(IPC_WINDOW_CONTROL.RENDER_CONTROL, IPC_WINDOW_CONTROL.MAXIMIZE),
            
            /*恢复*/
            unmaximize: () => ipcRenderer.send(IPC_WINDOW_CONTROL.RENDER_CONTROL, IPC_WINDOW_CONTROL.UNMAXIMIZE),
            
            /*关闭*/
            close: () => ipcRenderer.send(IPC_WINDOW_CONTROL.RENDER_CONTROL, IPC_WINDOW_CONTROL.CLOSE)
        };
    }
});
