/*
 * -------------------------------------------------------------------------
 * 渲染进程窗口管理器
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const ipcMain      = require('electron').ipcMain;
const windowSwitch = require('../window/switch');
const ipcCtl       = require('../ipc/dict').IPC_WINDOW_CONTROL;

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    /*获取主窗口*/
    let win = app.$windowMain;
    
    /*窗口控制*/
    ipcMain.on(ipcCtl.RENDER_CONTROL, (e, cmd) => {
        /*解析命令*/
        switch (cmd) {
            /*关闭*/
            case ipcCtl.CLOSE:
                windowSwitch.allWindowHide(app);
                win.hide();
                break;
            
            /*最小化*/
            case ipcCtl.MINIMIZE:
                win.minimize();
                break;
            
            /*最大化*/
            case ipcCtl.MAXIMIZE:
                win.maximize();
                break;
            
            /*还原*/
            case ipcCtl.UNMAXIMIZE:
                win.unmaximize();
                break;
        }
    });
    
    /*通知页面窗口最大化状态*/
    /*最大化*/
    win.on('maximize', () => win.webContents.send(ipcCtl.IS_MAXIMIZED, true));
    
    /*非最大化*/
    win.on('unmaximize', () => win.webContents.send(ipcCtl.IS_MAXIMIZED, false));
}

/**
 * 导出
 * @type {{initialize: initialize}}
 */
module.exports = {
    initialize
};
