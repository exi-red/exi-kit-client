/*
 * -------------------------------------------------------------------------
 * 渲染进程配置管理器
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const ipcMain     = require('electron').ipcMain;
const setting     = require('../../src/setting');
const IPC_SETTING = require('../../src/ipc/dict').IPC_SETTING;

/**
 * 将配置通知到渲染进程
 */
const rendererNotify = () => app.$windowMain.webContents.send(IPC_SETTING.NOTIFY, setting.read());

/**
 * 将配置更新结果通知到渲染进程
 * @param data
 */
const rendererUpdateNotify = data => app.$windowMain.webContents.send(IPC_SETTING.UPDATE_NOTIFY, data);

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    /*获取设置*/
    ipcMain.on(IPC_SETTING.GET, rendererNotify);
    
    /*更新设置*/
    ipcMain.on(IPC_SETTING.UPDATE, (e, data) => {
        rendererUpdateNotify(setting.update(data));
        rendererNotify();
    });
}

/**
 * 导出
 * @type {{rendererNotify: (function(): void), rendererUpdateNotify: (function(*=): void), initialize: initialize}}
 */
module.exports = {
    initialize,
    rendererNotify,
    rendererUpdateNotify
};
