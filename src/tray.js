/*
 * -------------------------------------------------------------------------
 * 托盘管理器
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const path         = require('path');
const Tray         = require('electron').Tray;
const menu         = require('./menu');
const windowSwitch = require('./window/switch');
const configs      = require('../package.json').configs;

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    /*初始化菜单*/
    menu.initialize(app);
    
    /*图标*/
    let iconNormal = path.resolve(__dirname, `../${configs.icon.normal}`);
    
    /*系统托盘*/
    app.$Tray = new Tray(iconNormal);
    
    /*设置图标*/
    app.$Tray.setImage(iconNormal);
    
    /*摘选菜单项*/
    let trayMenu = new app.$Menu();
    let appMenu  = app.$Menu.getApplicationMenu();
    trayMenu.append(appMenu.getMenuItemById(menu.menuId.showWindow));
    trayMenu.append(appMenu.getMenuItemById(menu.menuId.exit));
    
    /*设置托盘菜单*/
    app.$Tray.setContextMenu(trayMenu);
    
    /*点击切换窗口状态*/
    app.$Tray.on('click', windowSwitch.switchVisible);
    
    /*默认提示*/
    windowSwitch.setTrayEnable();
}

/**
 * 导出
 * @type {{initialize: initialize}}
 */
module.exports = {
    initialize
};
