/*
 * -------------------------------------------------------------------------
 * app事件管理器
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const shortcutGlobal = require('../shortcut/global');

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    singleInstance();
}

/**
 * 检测仅允许一个实例运行
 */
function singleInstance () {
    if (!app.requestSingleInstanceLock()) {
        app.quit();
        process.exit();
    }
    
    /* 运行第二个实例时 */
    app.on('second-instance', () => {
        if (app.$windowMain) {
            let win = app.$windowMain;
            if (win.isMinimized()) win.restore();
            win.focus();
            win.show();
        }
    });
}

/**
 * app完全退出
 */
function exit () {
    shortcutGlobal.unregister();
    app.exit();
}

/**
 * 导出
 * @type {{exit: exit, singleInstance: singleInstance, initialize: initialize}}
 */
module.exports = {
    initialize,
    singleInstance,
    exit
};
