/*
 * -------------------------------------------------------------------------
 * app单例管理
 * -------------------------------------------------------------------------
 */

/**
 * 获取app单例
 * @returns {Electron.App|*}
 */
const instance = () => {
    if (!process.$app) {
        const app = require('electron').app;
        
        /*记录默认appData目录，用于软件启动后清理*/
        /*具体原因查看/src/clearAppData.js中的说明*/
        app.$_appData = app.getPath('appData');
        
        /*挂载全局app模块*/
        process.$app = app;
    }
    return process.$app;
};

/**
 * 导出
 * @type {Electron.App|*}
 */
module.exports = instance();
