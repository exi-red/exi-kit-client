/*
 * -------------------------------------------------------------------------
 * app目录管理
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const path = require('path');

/**
 * 重写运行目录
 * @type {{crashDumps: string, userData: string, logs: string, appData: string}}
 */
const names = {
    appData   : 'appData',
    crashDumps: 'appData/crashDumps',
    userData  : 'appData/userData',
    logs      : 'appData/logs'
};

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    resetPaths();
}

/**
 * 获取目录
 * @param name
 * @returns {string}
 */
function getPath (name) {
    return names[name] ? path.resolve(path.dirname(app.getPath('exe')), names[name]) : '';
}

/**
 * 重写程序目录
 */
function resetPaths () {
    app.setPath('crashDumps', getPath('crashDumps'));
    app.setPath('userData', getPath('userData'));
    app.setPath('appData', getPath('appData'));
    app.setPath('logs', getPath('logs'));
    app.setAppLogsPath(getPath('logs'));
}

/**
 * 导出
 * @type {{resetPaths: resetPaths, getPath: (function(*): string|string), initialize: initialize}}
 */
module.exports = {
    initialize,
    resetPaths,
    getPath
};
