/*
 * -------------------------------------------------------------------------
 * 日志管理器
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const path          = require('path');
const log4js        = require('log4js');
const electronIsDev = require('electron-is-dev');
const configs       = require('../package.json').configs;
const appPaths      = require('./app/paths');

/* extodo 在setting中配置是否记录debug日志 */

/*日志保存路径*/
let logDir;

/*实例*/
let logger;

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    /*日志保存路径*/
    logDir = electronIsDev ? path.join(__dirname, '../logs') : appPaths.getPath('logs');
    
    /*配置*/
    log4js.configure({
        appenders: {
            
            /* 设置控制台输出 （默认日志级别是关闭的（即不会输出日志））*/
            out: {
                type: 'console' // 配置了这一项，除了会输出到日志文件，也会输出到控制台
            },
            
            /*设置每天：以日期为单位,数据文件类型，dataFiel 注意设置pattern，alwaysIncludePattern属性*/
            alldateFileLog   : {
                type                : 'dateFile',
                filename            : `${logDir}/log/log`,
                pattern             : 'yyyy-MM-dd.log',
                keepFileExt         : true,
                alwaysIncludePattern: true
                , maxLogSize        : configs.logMaxSize
                , backups           : 3
            },
            httpLog          : {
                type                : 'dateFile',
                filename            : `${logDir}/http/http`,
                pattern             : 'yyyy-MM-dd.log',
                keepFileExt         : true,
                alwaysIncludePattern: true
                , maxLogSize        : configs.logMaxSize
                , backups           : 3
            },
            renderProccessLog: {
                type                : 'dateFile',
                filename            : `${logDir}/renderer/renderer`,
                pattern             : 'yyyy-MM-dd.log',
                keepFileExt         : true,
                alwaysIncludePattern: true
                , maxLogSize        : configs.logMaxSize
                , backups           : 3
            },
            mainProccessLog  : {
                type                  : 'dateFile',
                filename              : `${logDir}/main/main`,
                pattern               : 'yyyy-MM-dd.log',
                keepFileExt           : true
                , alwaysIncludePattern: true
                , maxLogSize          : configs.logMaxSize
                , backups             : 3
            },
            crashLog         : {
                type                  : 'dateFile',
                filename              : `${logDir}/crash/crash`,
                pattern               : 'yyyy-MM-dd.log',
                keepFileExt           : true
                , alwaysIncludePattern: true
                , maxLogSize          : configs.logMaxSize
                , backups             : 3
            },
            
            /*错误日志 type:过滤类型logLevelFilter,将过滤error日志写进指定文件*/
            errorLog: {
                type                  : 'dateFile',
                filename              : `${logDir}/error/error`,
                pattern               : 'yyyy-MM-dd.log',
                keepFileExt           : true
                , alwaysIncludePattern: true
                , maxLogSize          : configs.logMaxSize
                , backups             : 3
            },
            error   : {
                type    : 'logLevelFilter',
                level   : 'error',
                appender: 'errorLog'
            }
        },
        
        /*不同等级的日志追加到不同的输出位置：appenders: ['out', 'allLog']  categories 作为getLogger方法的键名对应*/
        categories: {
            date    : {
                appenders: electronIsDev ? ['out', 'alldateFileLog'] : ['alldateFileLog'],
                level    : 'debug'
            },
            http    : {
                appenders: electronIsDev ? ['out', 'httpLog'] : ['httpLog'],
                level    : 'debug'
            },
            main    : {
                appenders: electronIsDev ? ['out', 'mainProccessLog'] : ['mainProccessLog'],
                level    : 'debug'
            },
            renderer: {
                appenders: electronIsDev ? ['out', 'renderProccessLog'] : ['renderProccessLog'],
                level    : 'debug'
            },
            crash   : {
                appenders: electronIsDev ? ['out', 'crashLog'] : ['crashLog'],
                level    : 'debug'
            },
            default : {
                appenders: electronIsDev ? ['out', 'alldateFileLog'] : ['alldateFileLog'],
                level    : 'debug'
            }
        }
    });
    
    /*获取实例*/
    logger = log4js.getLogger('default');
}

/**
 * 导出
 * @type {{warn: (function(*=): void), debug: (function(*=): void), log: (function(*=): void), initialize: initialize, error: (function(*=): void), info: (function(*=): void)}}
 */
module.exports = {
    initialize,
    log     : msg => logger.info(msg),
    debug   : msg => logger.debug(msg),
    info    : msg => logger.info(msg),
    warn    : msg => logger.warn(msg),
    error   : msg => logger.error(msg),
    default : log4js.getLogger('default'),
    http    : log4js.getLogger('http'),
    main    : log4js.getLogger('main'),
    renderer: log4js.getLogger('renderer'),
    crash   : log4js.getLogger('crash')
};
