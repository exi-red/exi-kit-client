/*
 * -------------------------------------------------------------------------
 * 默认appData目录清理
 * -------------------------------------------------------------------------
 * 因electron启动后，将重新设置appData目录，但默认appData目录仍会创建目录
 * 使用本工具的意图在每次启动后清理一次默认appData目录，保证不污染appData目录
 */

/*app对象实例*/
let app;

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    const packageJson = require('../package.json');
    const file        = require('../helper/file/noLog');
    const arch        = require('../helper/arch');
    const path        = require('path');
    
    /*清理默认appData目录*/
    file.delFiles(path.resolve(app.$_appData, `${packageJson.appName}-${arch.getArch()}`));
}

/**
 * 导出
 * @type {{initialize: initialize}}
 */
module.exports = {
    initialize
};
