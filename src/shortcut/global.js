/*
 * -------------------------------------------------------------------------
 * 全局快捷键管理器
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const globalShortcut = require('electron').globalShortcut;
const windowSwitch   = require('../window/switch');
const setting        = require('../setting');
const logger         = require('../logger');

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    setting.initialize(app);
    register();
}

/**
 * 注册全局快捷键
 * @param setKey
 * @returns {boolean}
 */
function register (setKey) {
    try {
        let bossKey = setKey || setting.read().bossKey;
        globalShortcut.register(bossKey, windowSwitch.switchVisible);
    }
    catch (e) {
        logger.error(e);
        return false;
    }
    return true;
}

/**
 * 更新
 * @param bossKey
 * @returns {boolean}
 */
function update (bossKey) {
    unregister();
    return register(bossKey);
}

/**
 * 注销全部全局快捷键
 */
function unregister () {
    globalShortcut.unregisterAll();
}

/**
 * 导出
 * @type {{unregister: unregister, update: (function(*=): boolean), initialize: initialize, register: ((function(*=): boolean)|*)}}
 */
module.exports = {
    initialize,
    register,
    update,
    unregister
};
