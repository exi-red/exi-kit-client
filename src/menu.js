/*
 * -------------------------------------------------------------------------
 * 菜单管理
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const Menu         = require('electron').Menu;
const appEvents    = require('./app/events');
const windowSwitch = require('./window/switch');

/**
 * 菜单标识
 * @type {{exit: string, refresh: string, showWindow: string, fullScreen: string}}
 */
const menuId = {
    refresh   : 'refresh',
    fullScreen: 'fullScreen',
    showWindow: 'showWindow',
    exit      : 'exit'
};

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
    
    app.$Menu = Menu;
    app.$Menu.setApplicationMenu(
        app.$Menu.buildFromTemplate([
            /*渲染进程中有效*/
            {
                id         : menuId.refresh,
                label      : '刷新',
                accelerator: 'F5',
                click      : (item, win) => win.reload()
            },
            
            /*渲染进程中有效*/
            {
                id         : menuId.fullScreen,
                label      : '全屏',
                accelerator: 'F11',
                click      : (item, win) => (win.isMaximized() ? win.unmaximize() : win.maximize())
            },
            
            {
                type: 'separator'
            },
            
            /*托盘菜单*/
            {
                id   : menuId.showWindow,
                label: '显示窗口',
                click: windowSwitch.allWindowShow
            },
            {
                id   : menuId.exit,
                label: '退出',
                click: appEvents.exit
            }
        ])
    );
}

/**
 * 导出
 * @type {{menuId: {exit: string, refresh: string, showWindow: string, fullScreen: string}, initialize: initialize}}
 */
module.exports = {
    initialize,
    menuId
};
