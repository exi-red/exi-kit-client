/*
 * -------------------------------------------------------------------------
 * 窗口（含托盘）显/隐切换
 * -------------------------------------------------------------------------
 */

/*app对象实例*/
let app;
const path          = require('path');
const electron      = require('electron');
const configs       = require('../../package.json').configs;
const BrowserWindow = electron.BrowserWindow;

/*图标*/
let iconNormal   = path.resolve(__dirname, `../../${configs.icon.normal}`);
let iconDisabled = path.resolve(__dirname, `../../${configs.icon.disabled}`);

/**
 * 初始化
 * @param appInstance
 */
function initialize (appInstance) {
    /*复写app实例*/
    app = appInstance;
}

/**
 * 控制窗口显/隐
 */
function switchVisible () {
    if (!app.$windowMain) return false;
    (!app.$windowMain.isFocused() || !app.$windowMain.isVisible()) ? allWindowShow() : allWindowHide();
}

/**
 * 隐藏全部窗口及托盘
 */
function allWindowHide () {
    const wins = BrowserWindow.getAllWindows();
    for (let i in wins) wins[i].hide();
    setTrayDisable();
}

/**
 * 显示所有窗口及托盘
 */
function allWindowShow () {
    const wins = BrowserWindow.getAllWindows();
    for (let i in wins) wins[i].show();
    setTrayEnable();
}

/**
 * 设置托盘焦点
 */
function setTrayEnable () {
    if (!app.$Tray) return;
    app.$Tray.setImage(iconNormal);
    app.$Tray.setToolTip('点击隐藏');
}

/**
 * 设置托盘失焦
 */
function setTrayDisable () {
    if (!app.$Tray) return;
    app.$Tray.setImage(iconDisabled);
    app.$Tray.setToolTip('点击显示');
}

/**
 * 导出
 * @type {{allWindowHide: allWindowHide, setTrayDisable: setTrayDisable, allWindowShow: allWindowShow, setTrayEnable: setTrayEnable, initialize: initialize, switchVisible: ((function(): (boolean|undefined))|*)}}
 */
module.exports = {
    initialize,
    switchVisible,
    allWindowHide,
    allWindowShow,
    setTrayEnable,
    setTrayDisable
};
