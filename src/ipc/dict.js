/*
 * -------------------------------------------------------------------------
 * IPC通信事件字典
 * -------------------------------------------------------------------------
 */

/**
 * 自动更新
 * @type {{RUN_SETUP_SUCCESS: string, NEW_VERSION: string, CHECK_ERROR: string, DOWNLOAD_ERROR: string, DOWNLOAD_PROGRESS: string, CHECK_INFO: string, DOWNLOAD_INFO: string, RENDER_CONFIRM: string, RENDER_REQUEST: string, PARSE_ERROR: string, RUN_SETUP_ERROR: string, PARSE_INFO: string}}
 */
const IPC_UPDATER = {
    RENDER_REQUEST   : 'updater:renderRequest',    /*渲染进程请求检测更新*/
    CHECK_INFO       : 'updater:getInfo',          /*检查更新提示*/
    CHECK_ERROR      : 'updater:getError',         /*检查更新错误*/
    PARSE_INFO       : 'updater:parseInfo',        /*解析提示*/
    PARSE_ERROR      : 'updater:parseError',       /*解析错误*/
    NEW_VERSION      : 'updater:newVersion',       /*发现新版本*/
    RENDER_CONFIRM   : 'updater:renderConfirm',    /*渲染进程确认更新*/
    DOWNLOAD_PROGRESS: 'updater:downloadProgress', /*下载进度*/
    DOWNLOAD_INFO    : 'updater:downloadInfo',     /*下载提示*/
    DOWNLOAD_ERROR   : 'updater:downloadError',    /*下载失败*/
    RUN_SETUP_ERROR  : 'updater:runSetupError',    /*启动安装包失败*/
    RUN_SETUP_SUCCESS: 'updater:runSetupSuccess'   /*启动安装包成功*/
};

/**
 * 配置管理器
 * @type {{NOTIFY: string, UPDATE_NOTIFY: string, GET: string, UPDATE: string}}
 */
const IPC_SETTING = {
    GET          : 'setting:get',           /*异步获取设置*/
    UPDATE       : 'setting:update',        /*异步更新设置*/
    NOTIFY       : 'setting:notify',        /*异步通知设置*/
    UPDATE_NOTIFY: 'setting:update:notify'  /*异步通知设置结果*/
};

/**
 * 窗口控制
 * @type {{RENDER_CONTROL: string, IS_MAXIMIZED: string, MINIMIZE: string, UNMAXIMIZE: string, CLOSE: string, MAXIMIZE: string}}
 */
const IPC_WINDOW_CONTROL = {
    CLOSE         : 'window:close',         /*关闭窗口*/
    MINIMIZE      : 'window:minimize',      /*最小化*/
    MAXIMIZE      : 'window:maximize',      /*最大化*/
    UNMAXIMIZE    : 'window:unmaximize',    /*还原*/
    IS_MAXIMIZED  : 'window:isMaximized',   /*是否最大化*/
    RENDER_CONTROL: 'window:renderControl'  /*是否最大化*/
};

/**
 * 对话框
 * @type {{SAVE_FILE: string, SAVE_FILE_RESULT: string, SELECT_FILES_RESULT: string, SELECT_FILES: string}}
 */
const IPC_DIALOG = {
    SAVE_FILE          : 'dialog:saveFile',         /*保存文件*/
    SAVE_FILE_RESULT   : 'dialog:saveFileResult',   /*保存文件结果*/
    SELECT_FILES       : 'dialog:selectFiles',      /*选择文件*/
    SELECT_FILES_RESULT: 'dialog:selectFilesResult' /*选择文件结果*/
};

/**
 * 插件
 * @type {{EXECUTIVE_RESULT: string, GETLISTS: string, EXECUTIVE: string, PUTLISTS: string}}
 */
const IPC_PLUGIN = {
    GETLISTS        : 'plugin:getlists',       /*请求插件列表*/
    PUTLISTS        : 'plugin:putlists',       /*通知插件列表*/
    EXECUTIVE       : 'plugin:executive',      /*请求执行*/
    EXECUTIVE_RESULT: 'plugin:executiveResult' /*执行结果*/
};

/**
 * webview
 * @type {{GET_SETTING: string, STATUS: {NEW_VERSION: string, NO_INSTALL: string, ON_INSTALL: string, INSTALLED: string, INSTALL_FAIL: string}, CONFIRM_INSTALL: string, PUT_SETTING: string, INSTALL_RESULT: string}}
 */
const IPC_WEBVIEW = {
    GET_SETTING    : 'webview:get_setting',          /*获取配置信息*/
    PUT_SETTING    : 'webview:put_setting',          /*通知配置信息*/
    CONFIRM_INSTALL: 'webview:confirm_install',      /*确认安装*/
    INSTALL_RESULT : 'webview:install_results',      /*安装结果*/
    STATUS         : {                               /*状态*/
        ON_INSTALL  : 'webview:status:on_install',   /*正在安装*/
        INSTALL_FAIL: 'webview:status:install_fail', /*安装失败*/
        NO_INSTALL  : 'webview:status:no_install',   /*未安装*/
        INSTALLED   : 'webview::status:installed',   /*已安装*/
        NEW_VERSION : 'webview::status:new_version'  /*发现新版本*/
    }
};

/**
 * 导出
 * @type {{IPC_SETTING: {NOTIFY: string, UPDATE_NOTIFY: string, GET: string, UPDATE: string}, IPC_UPDATER: {RUN_SETUP_SUCCESS: string, NEW_VERSION: string, CHECK_ERROR: string, DOWNLOAD_ERROR: string, DOWNLOAD_PROGRESS: string, CHECK_INFO: string, DOWNLOAD_INFO: string, RENDER_CONFIRM: string, RENDER_REQUEST: string, PARSE_ERROR: string, RUN_SETUP_ERROR: string, PARSE_INFO: string}, IPC_WINDOW_CONTROL: {RENDER_CONTROL: string, IS_MAXIMIZED: string, MINIMIZE: string, UNMAXIMIZE: string, CLOSE: string, MAXIMIZE: string}, IPC_DIALOG: {SAVE_FILE: string, SAVE_FILE_RESULT: string, SELECT_FILES_RESULT: string, SELECT_FILES: string}, IPC_PLUGIN: {EXECUTIVE_RESULT: string, GETLISTS: string, EXECUTIVE: string, PUTLISTS: string}, IPC_WEBVIEW: {GET_SETTING: string, STATUS: {NEW_VERSION: string, NO_INSTALL: string, ON_INSTALL: string, INSTALLED: string, INSTALL_FAIL: string}, CONFIRM_INSTALL: string, PUT_SETTING: string, INSTALL_RESULT: string}}}
 */
module.exports = {
    IPC_UPDATER,
    IPC_SETTING,
    IPC_DIALOG,
    IPC_PLUGIN,
    IPC_WEBVIEW,
    IPC_WINDOW_CONTROL
};
