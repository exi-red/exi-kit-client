/*
 * -------------------------------------------------------------------------
 * 快速判断工具
 * -------------------------------------------------------------------------
 */

/**
 * electron是否为开发环境
 * @returns {boolean}
 */
const electronDev = () => require('electron-is-dev');

/**
 * 判断是否为 function 类型
 * @param str
 * @returns {boolean}
 */
const func = str => typeof str === 'function';

/**
 * 判断是否为 undefined 类型
 * @param str
 * @returns {boolean}
 */
const undf = str => typeof str === 'undefined';

/**
 * 判断是否为 null 类型
 * @param str
 * @returns {boolean}
 */
const Null = str => str === null;

/**
 * 判断是否为 object 类型
 * @param str
 * @returns {boolean}
 */
const obj = str => typeof str === 'object';

/**
 * 判断是否为 boolean 类型
 * @param str
 * @returns {boolean}
 */
const bool = str => typeof str === 'boolean';

/**
 * 判断是否为 string 类型
 * @param str
 * @returns {boolean}
 */
const str = str => typeof str === 'string';

/**
 * 判断是否为 number 类型
 * @param str
 * @returns {boolean}
 */
const num = str => typeof str === 'number' && !isNaN(str);

/**
 * 判断是否为 json 类型
 * @param string
 * @return {boolean}
 */
const json = string => {
    try {
        if (str(string) && JSON.parse(string)) return true;
    }
    catch (e) { }
    return false;
};

/**
 * 是否为空
 * @param obj
 * @return {boolean}
 */
const empty = obj => {
    if (undf(obj) || Null(obj) || (obj === 0)) return true;
    if (obj === true) return false;
    if (num(obj)) obj += '';
    return !Boolean(Object.keys(obj).length);
};

/**
 * 导出
 * @type {{str: (function(*=): boolean), Null: (function(*): boolean), func: (function(*=): boolean), bool: (function(*=): boolean), obj: (function(*=): boolean), electronDev: (function(): boolean), num: (function(*=)), json: ((function(*=): boolean)|*), undf: (function(*=): boolean), empty: ((function(*=): boolean)|*)}}
 */
module.exports = {
    electronDev,
    empty,
    func,
    undf,
    Null,
    obj,
    bool,
    str,
    num,
    json
};
