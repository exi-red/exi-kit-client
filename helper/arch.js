/*
 * -------------------------------------------------------------------------
 * 基于package.json中的配置获取软件位数
 * -------------------------------------------------------------------------
 */
const packageJson = require('../package.json');

/*64位标识常量*/
const ARCH_X64 = 'x64';

/*32位标识常量*/
const ARCH_IA32 = 'ia32';

/*64位整数型常量*/
const ARCH_X64_INT = 64;

/*32位整数型常量*/
const ARCH_IA32_INT = 32;

/**
 * 基于 package.json 获取位数标识
 * @returns {string} x64|ia32
 */
function getArch () {
    return archIsIA32() ? ARCH_IA32 : ARCH_X64;
}

/**
 * 基于 package.json 获取位数整数型
 * @returns {number} 64|32
 */
function getArchInt () {
    return parseInt(packageJson.configs.electron.arch.toString(), 10);
}

/**
 * 基于 package.json 判断配置是否为64位
 * @returns {boolean}
 */
function archIsX64 () {
    return getArchInt() === ARCH_X64_INT;
}

/**
 * 基于 package.json 判断配置是否为32位
 * @returns {boolean}
 */
function archIsIA32 () {
    return getArchInt() === ARCH_IA32_INT;
}

/**
 * 导出
 * @type {{getArch: (function(): string), archIsIA32: (function(): boolean), getArchInt: (function(): number), archIsX64: (function(): boolean)}}
 */
module.exports = {
    getArch,
    getArchInt,
    archIsX64,
    archIsIA32
};
