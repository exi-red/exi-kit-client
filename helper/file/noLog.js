/*
 * -------------------------------------------------------------------------
 * 文件操作工具
 * -------------------------------------------------------------------------
 * 继承file工具
 * 引入日志管理器，可在electron环境下使用
 */
const file = require('./handler');

/*卸载日志管理器*/
file.unrequireLogger();

/*解构file*/
const { delFiles, copyFiles, copyFile, mkdir } = file;

/**
 * 导出
 * @type {{copyFile: ((function(*=, *=): boolean)|*), copyFiles: copyFiles, mkdir: ((function(*=): boolean)|*), delFiles: ((function(*=): (void))|*)}}
 */
module.exports = {
    delFiles,
    copyFiles,
    copyFile,
    mkdir
};

